const { Neutrino } = require('neutrino');

module.exports = Neutrino()
  .use(`${__dirname}/.neutrinorc.js`)
  .call('eslintrc');
