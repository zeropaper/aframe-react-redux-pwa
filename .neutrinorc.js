module.exports = {
  options: {
    output: 'public',
  },
  use: [
    [
      '@neutrinojs/airbnb',
      {
        eslint: {
          envs: ['es6', 'browser'],
          rules: {
            'react/prop-types': 'off',
          //   'object-curly-newline': 'off',
          //   'jsx-a11y/label-has-for': 'off',
          //   'jsx-a11y/click-events-have-key-events': 'off',
          //   'jsx-a11y/no-static-element-interactions': 'off',
          //   'jsx-a11y/anchor-is-valid': 'off',
          //   'react/forbid-prop-types': 'off',
          },
        },
      }
    ],
    ['@neutrinojs/react'],
    (neutrino) => {
      neutrino.config.plugins
        .delete('babel-minify');
    },
  ],

  env: {
    NODE_ENV: {
      production: {
        use: [
          [
            '@neutrinojs/pwa',
            {
              publicPath: process.env.PWA_PUBLIC_PATH || '/',
            },
          ],
          (neutrino) => {
            neutrino.config
              .entry('vendor')
                .add('react')
                .add('react-dom')
                .add('aframe-react')
                .add('aframe')
                .add('aframe-animation-component');
            neutrino.config
              .plugin('uglify')
              .use(require('uglifyjs-webpack-plugin'))
              .before('optimize-css')
          }
        ],
      },
    },
  },
};