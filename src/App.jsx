import { Scene } from 'aframe-react';
import React from 'react';

import Ball from './Ball';

export default class App extends React.Component {
  state = {
    balls: 1,
  };

  onBallClick = () => {
    this.setState({ balls: this.state.balls + 1 });
  };

  get balls() {
    const { balls } = this.state;
    const array = [];
    const angle = Math.PI * 2 * (1 / balls);
    const radius = 4;
    // const tilt = Math.PI * 1.5;

    for (let b = 0; b < balls; b += 1) {
      const x = Math.sin(0 + (angle * b)) * radius;
      const y = -2;
      const z = Math.cos(0 + (angle * b)) * radius;
      array.push((
        <Ball
          key={b}
          x={x}
          y={y}
          z={z}
          radius={0.5}
          events={{
            click: this.onBallClick,
          }}
        />
      ));
    }
    return array;
  }

  render() {
    // https://www.flickr.com/photos/hamburgerjung/43086302352/in/pool-equirectangular/
    return (
      <Scene
        cursor="rayOrigin: mouse"
        vr-mode-ui
        keyboard-shortcuts
        screenshot
        inspector
        background="color: rgba(64,64,64,1)"
      >
        <a-assets>
          <img
            alt="background"
            id="sky"
            crossOrigin="anonymous"
            src="https://farm2.static.flickr.com/1801/43086302352_053a8d6eb7_f.jpg"
          />
        </a-assets>
        <a-sky src="#sky" phi-length="360" />

        <a-camera>
          <a-cursor />
        </a-camera>

        <a-entity
          position="0 -8 4"
        >
          <a-box position="-1 0.5 -3" rotation="0 45 0" color="#4CC3D9" shadow="" />
          <a-sphere position="0 1.25 -5" radius="1.25" color="#EF2D5E" shadow="" />
          <a-cylinder position="1 0.75 -3" radius="0.5" height="1.5" color="#FFC65D" shadow="" />
          <a-plane position="2.5 0 -5" rotation="-90 0 0" width="10" height="10" color="transparent" shadow="" />
        </a-entity>
        {this.balls}
      </Scene>
    );
  }
}
