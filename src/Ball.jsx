import { Entity } from 'aframe-react';
import React from 'react';

export default class Ball extends React.Component {
  state = { over: false };

  onBallEnter = () => {
    this.setState({ over: true });
  };

  onBallLeave = () => {
    this.setState({ over: false });
  };

  render() {
    const {
      x = 0,
      y = 0,
      z = 0,
      radius = 1,
      events = {},
    } = this.props;
    const { over } = this.state;

    return (
      <Entity
        geometry={`primitive: sphere; radius: ${radius}; segmentsWidth: 8; segmentsHeight: 8`}
        material={`color: #aaa; shader: ${over ? 'flat' : 'standard'}; wireframe: ${over};`}
        position={`${x} ${y} ${z}"`}
        shadow=""
        visible
        events={{
          mouseenter: this.onBallEnter,
          mouseleave: this.onBallLeave,
          ...events,
        }}
      />
    );
  }
}
