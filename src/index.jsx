import 'babel-polyfill';
import 'aframe';
import 'aframe-animation-component';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';

import App from './App';

const rootEl = document.getElementById('root');
rootEl.style.position = 'absolute';
rootEl.style.width = '100%';
rootEl.style.height = '100%';

const load = () => render(
  (
    <AppContainer>
      <App />
    </AppContainer>
  ),
  rootEl,
);

if (module.hot) {
  module.hot.accept('./App', load);
}

load();
